package atvdPoo;

public class Hor_Seg_Min {
	
private byte hora;
private byte minuto;
private byte segundo;
	
	public void setHora(byte hora) {
		
		if(hora >= 0 && hora <= 23) {
			this.hora = hora;
			
		}
		
	}
	
	public byte getHora() {
		return this.hora;
	}

	
	public void setminuto(byte minuto) {
		
		if(minuto >= 0 && minuto <= 59) {
			this.minuto = minuto;
			
		}
		
	}

	public byte getminuto() {
	return this.minuto;
}	
	
public void setsegundo(byte segundo) {
		
		if(segundo >= 0 && segundo <= 59) {
			this.segundo = segundo;
			
		}
		
	}

	public byte getsegundo() {
	return this.segundo;
}	
	
	public String toString() {
		return getHora() + ":" + getminuto() + ":" + getsegundo();
	}
		
}


